﻿using System;
using Common;

namespace ConsoleApp2
{
    class Program
    {
        private static MainViewModel _mainViewModel;

        static void Main(string[] args)
        {
            var email = Console.ReadLine();
            var password = Console.ReadLine();

            _mainViewModel = new MainViewModel(new ConsoleAlertDisplay());
            _mainViewModel.VerifyData(email, password);

            Console.ReadKey();
        }
    }

    internal class ConsoleAlertDisplay : IAlertDisplayer
    {
        public void DisplayAlert(string title, string message)
        {
            Console.WriteLine(title);
            Console.WriteLine(message);
        }
    }

    //public class MainViewModel
}
