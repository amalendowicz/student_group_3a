﻿using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Common;

namespace Android
{
    [Activity(Label = "Android", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private MainViewModel _mainViewModel;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            _mainViewModel = new MainViewModel(new DroidAlertDisplayer(this));
            var button = FindViewById<Button>(Resource.Id.button);
            var editTextEmail = FindViewById<EditText>(Resource.Id.edittext_email);
            var editTextPassword = FindViewById<EditText>(Resource.Id.edittext_password);

            button.Click += (sender, args) => _mainViewModel.VerifyData(editTextEmail.Text, editTextPassword.Text);
        }
    }

    public class DroidAlertDisplayer : IAlertDisplayer
    {
        private readonly Context _context;

        public DroidAlertDisplayer(Context context)
        {
            _context = context;
        }

        public void DisplayAlert(string title, string message)
        {
            new AlertDialog.Builder(_context)
                .SetTitle(title)
                .SetMessage(message)
                .Create()
                .Show();
        }
    }
}

