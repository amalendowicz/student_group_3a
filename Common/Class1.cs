﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Common.Annotations;

namespace Common
{
    public interface IAlertDisplayer
    {
        void DisplayAlert(string title, string message);
    }

    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly IAlertDisplayer _alertDisplayer;
        private string _email;
        private string _password;

        public MainViewModel(IAlertDisplayer alertDisplayer)
        {
            _alertDisplayer = alertDisplayer;
            Email = "artur.malendowicz@insanelab.com";
        }

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value; 
                OnPropertyChanged();
            }
        }

        public ICommand VerifyDataCommand => new Command(VerifyData);

        public void VerifyData()
        {
            if (Email == "a" && Password == "b")
            {
                _alertDisplayer.DisplayAlert("Sukces", "Podane dane są poprawne");
            }
            else
            {
                _alertDisplayer.DisplayAlert("Błąd", "Podane dane są nieprawidłowe");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Command : ICommand
    {
        private readonly Action _action;

        public Command(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action.Invoke();            
        }

        public event EventHandler CanExecuteChanged;
    }
}
